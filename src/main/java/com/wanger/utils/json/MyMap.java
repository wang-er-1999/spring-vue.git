package com.wanger.utils.json;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: wanger
 * @Date: 2023/9/27 14:47
 * @Description: 自定义map，实现简化json构造
 */
public class MyMap extends HashMap<String, Object> {
    private static final long serialVersionUID = 3149678103625048002L;

    private MyMap() {
    }

    public static MyMap create() {
        return new MyMap();
    }

    public MyMap pu(String key, Object data) {
        super.put(key, data);
        return this;
    }

    public Object[] toArr() {
        MyList list = MyJson.list();
        for (Map.Entry<String, Object> entry : this.entrySet()) list.ad(entry);
        return list.toArr();
    }


    public Map<String, Object> toMap() {
        return this;
    }
}
