package com.wanger.utils.json;

/**
 * @Author: wanger
 * @Date: 2023/9/28 9:30
 * @Description: 创建自定义list或者map
 */
public class MyJson {

    /**
     * 静态创建自定义list实例
     *
     * @return 实例对象
     */
    public static MyList list() {
        return MyList.create();
    }

    /**
     * 静态创建自定义map实例
     *
     * @return 实例对象
     */
    public static MyMap map() {
        return MyMap.create();
    }
}
