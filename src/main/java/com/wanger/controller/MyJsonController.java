package com.wanger.controller;

import com.wanger.server.IHomeServer;
import com.wanger.utils.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: wanger
 * @Date: 2023/9/17 23:48
 * @Description:
 */
@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/myJson")
public class MyJsonController {

    @Resource
    private IHomeServer iHomeServer;

    @GetMapping("/hello")
    public String hello() {
        return "服务调用测试";
    }

    /**
     * 要求一个接口返回首页所有数据，
     * 该数据包含：描述，柱状图，折线图，饼图，列表
     *
     * @return 封装后的首页数据
     */
    @GetMapping("/homePage")
    public R<Object> homePage() {
        return R.get(iHomeServer.homePage());
    }

}
