package com.wanger.utils.json;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wanger
 * @Date: 2023/9/27 14:49
 * @Description: 自定义list，实现简化json构造
 */
public class MyList extends ArrayList<Object> {
    private static final long serialVersionUID = -6352745866130164444L;

    private MyList() {
    }

    public static MyList create() {
        return new MyList();
    }

    public MyList ad(Object data) {
        super.add(data);
        return this;
    }

    public Object[] toArr() {
        return super.toArray();
    }

    public List<Object> toList() {
        return this;
    }

}
