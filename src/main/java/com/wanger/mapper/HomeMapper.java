package com.wanger.mapper;

import cn.hutool.core.lang.UUID;
import com.wanger.utils.json.MyJson;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author: wanger
 * @Date: 2023/9/27 23:27
 * @Description:
 */
@Component
public class HomeMapper {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    //客户信息
    public List<Object> getList() {
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(MyJson.map()
                    .pu("id", UUID.randomUUID().toString())
                    .pu("date", sdf.format(new Date()))
                    .pu("name", i % 3 == 0 ? "王二" : "张三")
                    .pu("address", i % 2 == 0 ? "山东曹县666号路" : "阳关大道物业小区2304号")
            );
        }
        return list;
    }

    //用户信息
    public Object getUserInfo() {
        return MyJson.map()
                .pu("userName", "wanger")
                .pu("role", "管理员")
                .pu("lastLoginTime", sdf.format(new Date(System.currentTimeMillis() - 7 * 24 * 3600 * 1000L)))
                ;
    }

    //指标信息
    public Object getIndexes() {
        return MyJson.list()
                .ad(MyJson.map()
                        .pu("color", "#2ec7c9")
                        .pu("value", 1200)
                        .pu("icon", "success")
                        .pu("name", "今日支付订单")
                )
                .ad(MyJson.map()
                        .pu("color", "#ffb980")
                        .pu("value", 120)
                        .pu("icon", "star-on")
                        .pu("name", "今日收藏订单")
                )
                .ad(MyJson.map()
                        .pu("color", "#5ab1ef")
                        .pu("value", 12)
                        .pu("icon", "s-goods")
                        .pu("name", "今日取消订单")
                )
                .ad(MyJson.map()
                        .pu("color", "#2ec7c9")
                        .pu("value", 1100)
                        .pu("icon", "success")
                        .pu("name", "今日退款订单")
                )
                .ad(MyJson.map()
                        .pu("color", "#ffb980")
                        .pu("value", 100)
                        .pu("icon", "star-on")
                        .pu("name", "本月支付订单")
                )
                .ad(MyJson.map()
                        .pu("color", "#5ab1ef")
                        .pu("value", 1)
                        .pu("icon", "s-goods")
                        .pu("name", "本月退款订单")
                );
    }

    //柱状图
    public Object marketBar() {
        return MyJson.map()
                .pu("legend",
                        MyJson.map().pu("data", new String[]{"今日销量", "昨日销量"})
                )
                .pu("xAxis",
                        MyJson.map().pu("data", new String[]{"华为", "vivo", "oppo", "ipone", "小米", "三星"})
                )
                .pu("series", MyJson.list()
                        .ad(MyJson.map()
                                .pu("name", "今日销量")
                                .pu("type", "bar")
                                .pu("data", new int[]{5, 20, 36, 10, 10, 20})
                        )
                        .ad(MyJson.map()
                                .pu("name", "昨日销量")
                                .pu("type", "bar")
                                .pu("data", new int[]{10, 18, 34, 8, 12, 21})
                        )
                )
                ;
    }

    //饼图
    public Object marketPie() {
        return MyJson.list()
                .ad(MyJson.map()
                        .pu("value", 1049)
                        .pu("name", "成交订单量")
                )
                .ad(MyJson.map()
                        .pu("value", 735)
                        .pu("name", "退款订单量")
                )
                .ad(MyJson.map()
                        .pu("value", 580)
                        .pu("name", "浏览量")
                )
                .ad(MyJson.map()
                        .pu("value", 484)
                        .pu("name", "加购量")
                )
                .ad(MyJson.map()
                        .pu("value", 300)
                        .pu("name", "预购量")
                )
                ;
    }
}
