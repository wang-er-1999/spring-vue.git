package com.wanger;

import com.alibaba.fastjson2.JSON;
import com.wanger.utils.json.MyJson;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: wanger
 * @Date: 2023/9/27 21:00
 * @Description:
 */
public class Test {
    /*
{
    "name": "SO JSON在线",
    "url": "https://www.sojson.com",
    "address": {
        "city": "北京",
        "country": "中国"
    },
    "domain_list": [
        {
            "name": "ICP备案查询",
            "url": "https://icp.sojson.com"
        },
        {
            "name": "JSON在线解析",
            "url": "https://www.sojson.com"
        },
        {
            "name": "房贷计算器",
            "url": "https://fang.sojson.com"
        }
    ]
}
    * */
    public static void main(String[] args) {
        System.out.println("传统方式构造json：");
        System.out.println(JSON.toJSONString(traditional()));
        System.out.println("通过自定义工具优雅构造json");
        System.out.println(JSON.toJSONString(elegantBuilder()));
    }

    private static Object traditional() {
        Map<String, Object> map1 = new LinkedHashMap<>();
        Map<String, Object> map2 = new LinkedHashMap<>();
        Map<String, Object> map3 = new LinkedHashMap<>();
        Map<String, Object> map4 = new LinkedHashMap<>();
        Map<String, Object> map5 = new LinkedHashMap<>();
        List<Object> list1 = new ArrayList<>();
        map1.put("name", "SO JSON在线");
        map1.put("url", "https://www.sojson.com");
        map2.put("city", "北京");
        map2.put("country", "中国");
        map1.put("address", map2);
        map3.put("name", "ICP备案查询");
        map3.put("url", "https://icp.sojson.com");
        list1.add(map3);
        map4.put("name", "JSON在线解析");
        map4.put("url", "https://www.sojson.com");
        list1.add(map4);
        map5.put("name", "房贷计算器");
        map5.put("url", "https://fang.sojson.com");
        list1.add(map5);
        map1.put("domain_list", list1);
        return map1;
    }

    private static Object elegantBuilder() {
        return MyJson.map()
                .pu("name", "SO JSON在线")
                .pu("url", "https://www.sojson.com")
                .pu("address", MyJson.map()
                        .pu("city", "北京")
                        .pu("country", "中国")
                )
                .pu("domain_list", MyJson.list()
                        .ad(MyJson.map()
                                .pu("name", "ICP备案查询")
                                .pu("url", "https://icp.sojson.com")
                        )
                        .ad(MyJson.map()
                                .pu("name", "JSON在线解析")
                                .pu("url", "https://www.sojson.com")
                        )
                        .ad(MyJson.map()
                                .pu("name", "房贷计算器")
                                .pu("url", "https://fang.sojson.com")
                        )
                );
    }
}
