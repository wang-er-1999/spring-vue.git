import Vue from "vue";
import Vuex from 'vuex';

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        count: 0,
        isCollapse: true
    },
    mutations: {
        increment(state) {
            state.count++
        },
        MENUHANDLER(state) {
            state.isCollapse = !state.isCollapse
        }
    }
})
export default store