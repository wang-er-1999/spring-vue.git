package com.wanger.server.impl;

import com.wanger.mapper.HomeMapper;
import com.wanger.server.IHomeServer;
import com.wanger.utils.json.MyJson;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author: wanger
 * @Date: 2023/9/27 22:48
 * @Description:
 */
@Service
public class HomeServer implements IHomeServer {
    @Resource
    private HomeMapper homeMapper;

    /**
     * 描述，柱状图，饼图，列表等数据
     *
     * @return data
     */
    @Override
    public Object homePage() {
        return MyJson.map()
                .pu("userInfo", homeMapper.getUserInfo())
                .pu("customerInformation", MyJson.map()
                        .pu("head", "客户信息")
                        .pu("list", homeMapper.getList())
                )
                .pu("indexes", homeMapper.getIndexes())
                .pu("marketBar", MyJson.map()
                        .pu("title", "销售柱状图")
                        .pu("data", homeMapper.marketBar())
                )
                .pu("marketPie", MyJson.map()
                        .pu("title", "销售饼图")
                        .pu("data", homeMapper.marketPie())
                );
    }
}
