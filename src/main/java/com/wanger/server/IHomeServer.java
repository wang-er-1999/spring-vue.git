package com.wanger.server;

/**
 * @Author: wanger
 * @Date: 2023/9/27 22:48
 * @Description:
 */
public interface IHomeServer {

    Object homePage();
}
