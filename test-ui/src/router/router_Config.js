import VueRouter from 'vue-router'
import IndexPage from '../views/IndexPage.vue'

export default new VueRouter({
    routes:[
        {
            path : "/",
            name: 'home',
            component: IndexPage
        }
    ]
})